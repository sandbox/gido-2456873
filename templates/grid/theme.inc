<?php
/**
 * @file
 * Theme functions.
 */

/**
 * Proprocess views to precalculate bootstrap classes and more.
 */
function template_preprocess_views_bootstrap_responsive_grid_plugin_style(&$vars) {
  $view     = $vars['view'];
  $options  = $view->style_plugin->options;

  $column_breakpoints = array();
  $column_classes = array();
  foreach ($view->style_plugin->getColumnBreakpoints() as $size_key => $size) {
    // Check if enabled.
    if ($options['item_per_row_' . $size_key]) {
      $column_type = 12 / (int) $options['item_per_row_' . $size_key];
      $column_classes[] = ' col-' . $size_key . '-' . $column_type;
      $column_breakpoints[$size_key] = (int) $options['item_per_row_' . $size_key];
    }
  }

  $vars['columnBreakpoints'] = $column_breakpoints;
  $vars['column_classes'] = implode(' ', $column_classes);
}
